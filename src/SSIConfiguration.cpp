/*
 SSIConfiguration.cpp
 Author: G. Lehmann
*/
#include "DFSubSystemItem/SSIConfiguration.h"
#include <iostream>

using namespace ROS;

SSIConfiguration::SSIConfiguration(const char* confStream) {
  //Gets a stream and transforms it into a SSIConfiguration
  string s(confStream);
  std::string::size_type br = 0;
  string searcher("<<<");
  while((br = s.find(searcher)) != string::npos) {
    string key(s, 0, br);
    s.erase(0,br+searcher.size());
    Config::T_ConfigContainer cc;
    int index = 0;
    while(((br = s.find("endConfig"))!= string::npos) 
	  && (br < s.find("endMapElement"))) {
      string confStream(s, 0, br);
      cc.push_back(Config::New(confStream.c_str()));
      index++;
      s.erase(0, br+strlen("endConfig")+searcher.size());
    }
    s.erase(0, strlen("endMapElement"));    
    this->setConfig(key, cc);
  }
}

Config::T_ConfigContainer SSIConfiguration::getConfig(string key) {
  if(m_ssiConfig.find(key) != m_ssiConfig.end())
    return (m_ssiConfig[key]);
  else {
    throw(SSIConfigurationException(SSIConfigurationException::UNKNOWN_CONFIG_ERROR, key));
  }
}

void SSIConfiguration::setConfig(string key,  Config::T_ConfigContainer conf) {
  m_ssiConfig[key] = conf; 
  return;
}


string SSIConfiguration::serialize() {
  
  string s;
   for (map<string,Config::T_ConfigContainer>::iterator iter=m_ssiConfig.begin();
        iter != m_ssiConfig.end(); iter++) {
     s+= (*iter).first;
     s+= "<<<";
     for(u_int index = 0; index < (*iter).second.size(); index++) {
       s+=((*iter).second)[index]->serialize();
       s+= "<<<";
     }
     s+="endMapElement";
   }
  return s;
}

void SSIConfiguration::vectorize(string key, vector<string> & StringArray) {
  for (Config::VectorIterator VI = m_ssiConfig[key].begin(); 
       VI !=  m_ssiConfig[key].end(); ++VI) {
    (*VI)->vectorize(StringArray);
  }
  return;
}
void SSIConfiguration::keys(vector<string> & StringArray) {
  for (SSIConfiguration::SSIConfigIterator LI = m_ssiConfig.begin();
       LI != m_ssiConfig.end(); ++LI) {
    StringArray.push_back(LI->first);
  }
  return;
}

void SSIConfiguration::dump() {
	std::cout <<"\n*******************************\n";
  for (map<string,Config::T_ConfigContainer>::iterator iter=m_ssiConfig.begin();
       iter != m_ssiConfig.end(); iter++) {
	  std::cout << "SSIConfiguration: key --> " << (*iter).first << ": \n";
    for(u_int index = 0; index < (*iter).second.size(); index++) {
      (*iter).second[index]->dump();
    }
  }
}
