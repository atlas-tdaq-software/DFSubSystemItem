/*--------------------------------------
   SubSystemItem default implementation

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/


#include "DFSubSystemItem/SubSystemItem.h"

SubSystemItem::SubSystemItem(string name) { 
  m_getInfoParameters = 0;
  m_runParameters = 0;
  m_configuration = 0;
  this->setActive(true);
  this->setRestartable(false);
  this->setEssential(true);
  this->setName(name.c_str());
}
SubSystemItem::~SubSystemItem() {}

string SubSystemItem::getConfigPlugin(){
  return m_pluginName;
}
void SubSystemItem::setConfigPlugin(std::string & pluginName){
  m_pluginName = pluginName;
}

void SubSystemItem::init(){}
void SubSystemItem::uninit() {}

void SubSystemItem::updateConfig(SSIConfiguration* conf){
  m_configuration = conf;
  }
void SubSystemItem::updateRunConfig(SSIConfiguration* runconf){
  m_runParameters = runconf;
}
void SubSystemItem::updateStatistics(SSIConfiguration* conf){
  m_getInfoParameters = conf;
}

SSIConfiguration* SubSystemItem::getConfig() {
  return m_configuration;
}

SSIConfiguration* SubSystemItem::getRunConfig() {
  return m_runParameters;
}

SSIConfiguration* SubSystemItem::getStatistics() {
  return m_getInfoParameters;
}

void SubSystemItem::clearConfig() { 
  if (m_configuration) {
    delete m_configuration;
    m_configuration = 0;
  }
}

void SubSystemItem::clearRunConfig(){
  if (m_runParameters) {
    delete m_runParameters;
    m_runParameters = 0;
  }
}

void SubSystemItem::clearStatistics() {
  if (m_getInfoParameters) { 
    delete m_getInfoParameters;
    m_getInfoParameters = 0;
  }
}

void SubSystemItem::setRestartable(bool restartable) {
  m_restartable = restartable;
}
bool SubSystemItem::restartable() {
  return m_restartable;
}
void SubSystemItem::setEssential(bool essential) {
  m_essential = essential;
}
bool SubSystemItem::essential() {
  return m_essential;
}
void SubSystemItem::setActive(bool active) {
  m_active = active;
}
bool SubSystemItem::active() {
  return m_active;
}
