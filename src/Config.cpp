// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, G. Lehmann
//
//  $Log$
//  Revision 1.28  2008/11/18 12:36:30  gcrone
//  Modifications for compilation on 64bit platform
//
//  Revision 1.27  2008/05/18 09:05:03  gcrone
//  Look for keys with both upper and lower case first letter
//
//  Revision 1.26  2007/11/07 19:05:22  gcrone
//  Remove 'using namespace' and specify std:: where necessary
//
//  Revision 1.25  2007/09/27 16:39:32  gcrone
//  Add size() method
//
//  Revision 1.24  2007/09/24 12:52:23  gcrone
//  Add 64bit methods
//
//  Revision 1.23  2006/10/31 16:49:48  gcrone
//  New ers::Issue based exceptions
//
//  Revision 1.22  2006/08/17 17:44:40  gcrone
//  Accidently included some updates for moving to ERS in last commit
//
//  Revision 1.21  2006/08/17 17:23:35  gcrone
//  Put Config in ROS namespace
//
//  Revision 1.20  2006/08/01 16:38:12  gcrone
//  New isKey method to check if a key exists
//
//  Revision 1.19  2005/10/19 16:52:50  gcrone
//  Remove string "Config:" from dump()
//
//  Revision 1.18  2005/08/01 15:36:59  gcrone
//  Check conversions from strings
//
//  Revision 1.17  2005/04/20 15:11:41  gcrone
//  Removed debug cout from getString.
//
//  Revision 1.16  2005/03/11 09:58:30  gcrone
//  Add get/set for double
//
//  Revision 1.15  2005/01/13 16:56:12  gcrone
//  Add index argument to get/set methods.
//  Add getPointer/setPointer methods.
//
//  Revision 1.14  2004/11/24 19:02:11  gcrone
//  Get integer methods (getInt, getUInt, getLUInt) interpret values as
//  hexadecimal if they are preceded by 0x
//
//  Revision 1.13  2004/09/10 10:39:37  akazarov
//  added Config::set(const string key, const long int value) method, needed for new RunParams where beamEnergy has s32 type
//
//  Revision 1.12  2004/02/11 11:26:04  glehmann
//  adapt to online-21
//
//  Revision 1.11  2003/12/18 15:23:20  glehmann
//  christmas cleaning up of print statements
//
//  Revision 1.10  2003/04/03 08:27:57  glehmann
//  got rid of all harmful ends
//
//  Revision 1.9  2003/04/01 09:28:28  glehmann
//  removed ifdef for strstream
//
//  Revision 1.8  2003/03/17 07:59:46  glehmann
//  more auxiliary methods
//
//  Revision 1.7  2003/03/13 15:06:04  pyannis
//  Sonia: added aux functions
//
//  Revision 1.6  2003/01/30 14:39:05  gcrone
//  Throw exception if item not found
//
//  Revision 1.5  2002/11/27 16:47:02  gcrone
//  Added getUint() and reset()
//
//  Revision 1.4  2002/11/26 15:39:14  gcrone
//  Added float get and set methods.
//
//  Revision 1.3  2002/11/01 16:38:12  glehmann
//  added ControlledDeviceSkeleton.cpp and integration work
//
//  Revision 1.2  2002/10/17 16:56:31  glehmann
//  Debugged encoding and decoding of parameters
//
//
// //////////////////////////////////////////////////////////////////////
# include <sstream>
#include <string>
#include <ctype.h>

#include "DFSubSystemItem/Config.h"
#include "DFSubSystemItem/ConfigException.h"
#include "ROSUtilities/ROSErrorReporting.h"

using ROS::Config;
using std::string;
using std::istringstream;
using std::ostringstream;

Config::Config() {
}

Config::Config(const char* confStream) {
  //Gets a stream and transforms it into a Configuration
  string s(confStream);
  string searcher("<<<");
  std::string::size_type br = 0;
  while((br = s.find(searcher)) != string::npos) {
    string key(s, 0, br);
    s.erase(0, br+searcher.size());
    if(((br = s.find(searcher))!= string::npos) ) {
      string value(s, 0, br);
      s.erase(0, br+searcher.size());
      this->set(key, value); 
    }
  }
}

string Config::serialize() {
  // Transforms the Configuration container into a bytestream
  string s;
   for (std::map<string,string>::iterator iter=m_map.begin();
        iter != m_map.end(); iter++) {
     s+= (*iter).first + "<<<" + (*iter).second +"<<<" ;
   }

   s+= "endConfig";
   //  s+= "<<<";
   return s;
}

void Config::vectorize(std::vector<string> & StringArray) {
  for (Config::ConfigIterator MI = m_map.begin(); 
       MI != m_map.end(); ++MI) {
    StringArray.push_back((MI->first) + " = " + (MI->second) + "\0");
  }
  return;
}


int Config::getInt(const string key, const int index)
{
   string item=getString(key, index);
   istringstream sStream(item);


   if (item.substr(0, 2) == "0x") {
      sStream >> std::hex ;
   }

   int result;
   if (sStream >> result) {
      return (result);
   }
   else {
      CREATE_ROS_EXCEPTION(convException, ConfigException, BAD_CONVERSION_ERROR, 
                           " retrieving "<< key << " (value=" << item << ") as int");
      throw(convException);
   }
}


unsigned int Config::getUInt(const string key, const int index)
{
   string item=getString(key, index);
   istringstream sStream(item);

   if (item.substr(0, 2) == "0x") {
      sStream >> std::hex ;
   }

   unsigned int result;
   if (sStream >> result) {
      return (result);
   }
   else {
      CREATE_ROS_EXCEPTION(convException, ConfigException, BAD_CONVERSION_ERROR, 
                           " retrieving "<< key << " (value=" << item << ") as unsigned int");
      throw(convException);
   }
}

long unsigned int Config::getLUInt(const string key, const int index)
{
   string item=getString(key, index);
   istringstream sStream(item);

   if (item.substr(0, 2) == "0x") {
      sStream >> std::hex ;
   }

   long unsigned int result;
   sStream >> result;
   return (result);
}

int64_t Config::getS64(const string key, const int index)
{
   string item=getString(key, index);
   istringstream sStream(item);

   if (item.substr(0, 2) == "0x") {
      sStream >> std::hex ;
   }

   int64_t result;
   sStream >> result;
   return (result);
}

uint64_t Config::getU64(const string key, const int index)
{
   string item=getString(key, index);
   istringstream sStream(item);

   if (item.substr(0, 2) == "0x") {
      sStream >> std::hex ;
   }

   uint64_t result;
   sStream >> result;
   return (result);
}

float Config::getFloat(const string key, const int index)
{
   string item=getString(key, index);
   istringstream sStream(item);

   float result;
   if (sStream >> result) {
      return (result);
   }
   else {
      CREATE_ROS_EXCEPTION(convException, ConfigException, BAD_CONVERSION_ERROR, 
                           " retrieving "<< key << " (value=" << item << ") as float");
      throw(convException);
   }
}

double Config::getDouble(const string key, const int index)
{
   string item=getString(key, index);
   istringstream sStream(item);

   double result;
   if (sStream >> result) {
      return (result);
   }
   else {
      CREATE_ROS_EXCEPTION(convException, ConfigException, BAD_CONVERSION_ERROR, 
                           " retrieving "<< key << " (value=" << item << ") as double");
      throw(convException);
   }
}

Config::ConfigIterator Config::lookup(const string& key, const int index)
{
   string lookupKey=key;
   if (index != -1) {
      ostringstream tStream;
      tStream << key << "#" << index;
      lookupKey=tStream.str();
   }

   Config::ConfigIterator result=m_map.find(lookupKey);
   if (result==m_map.end()) {
      char initial=lookupKey[0];
      initial=toupper(initial);
      if (initial==lookupKey[0]) {
         initial=tolower(initial);
      }
      
      lookupKey[0]=initial;
      result=m_map.find(lookupKey);
   }
   return(result);
}

bool Config::isKey(const string& key, const int index)
{
   ConfigIterator iter=lookup(key, index);
   return (iter != m_map.end()) ;
}
string Config::getString(const string key, const int index)
{
   ConfigIterator iter=lookup(key, index);
   if (iter == m_map.end()) {
      CREATE_ROS_EXCEPTION(keyException, ConfigException, UNKNOWN_KEY_ERROR, key);
      throw(keyException);
   }
   return((*iter).second);
}

bool Config::getBool(const string key, const int index)
{
   string value=getString(key, index);
   if (value[0]=='1' || value[0]=='t') {
      return (true);
   }
   else {
      return (false);
   }
}

const string Config::genKey(const string key, const int index)
{
   if (index == -1) {
      return key;
   }
   else {
      ostringstream sStream;
      sStream << key << "#" << index;
      return sStream.str();
   }
}
void Config::set(const string key, const int value, const int index)
{
   ostringstream sStream;
   sStream << value;
   m_map[genKey(key, index)]=sStream.str();
}

void Config::set(const string key, const unsigned int value, const int index)
{
   ostringstream sStream;
   sStream << value;
   m_map[genKey(key, index)]=sStream.str();
}


void Config::set(const string key, const int64_t value, const int index)
{
   ostringstream sStream;
   sStream << value;
   m_map[genKey(key, index)]=sStream.str();
}

void Config::set(const string key, const uint64_t value, const int index)
{
   ostringstream sStream;
   sStream << value;
   m_map[genKey(key, index)]=sStream.str();
}

void Config::set(const string key, const float value, const int index)
{
   ostringstream sStream;
   sStream.precision(10);
   sStream << value;
   m_map[genKey(key, index)]=sStream.str();
}

void Config::set(const string key, const double value, const int index)
{
   ostringstream sStream;
   sStream.precision(12);
   sStream << value;
   m_map[genKey(key, index)]=sStream.str();
}

void Config::set(const string key, const string value, const int index)
{
   m_map[genKey(key, index)]=value;
}

void Config::reset(void)
{
   m_map.clear();
}

size_t Config::size()
{
   return m_map.size();
}

void Config::dump(void) 
{
  if(m_map.size()>0) {
    std::cout << "\n-------------------------------------------\n";
    for (std::map<string,string>::iterator iter=m_map.begin();
         iter != m_map.end(); iter++) {
      std::cout << " key --> " << (*iter).first << "\t value --> " << (*iter).second << std::endl ;
    }
    std::cout << "-------------------------------------------\n";
  }
}
