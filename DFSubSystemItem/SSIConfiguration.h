// -*- c++ -*-
// Author: G. Lehmann 

#ifndef SSICONFIGURATION_H
#define SSICONFIGURATION_H

#include <vector>
#include <string>
#include <DFSubSystemItem/Config.h>

#include "DFExceptions/ROSException.h"
using namespace std;

namespace ROS {
   class SSIConfigurationException : public  ROSException
   {
   public:
      enum ErrorCode {UNKNOWN_CONFIG_ERROR};
      string getErrorString(unsigned int errorId) const;

      SSIConfigurationException(ErrorCode error);
      SSIConfigurationException(ErrorCode error, string description);

protected:
   virtual ers::Issue * clone() const { return new SSIConfigurationException( *this ); }

   static const char * get_uid() { return "ROS::SSIConfigurationException"; }
   virtual const char * get_class_name() const { return get_uid(); }

   };

   inline SSIConfigurationException::SSIConfigurationException(SSIConfigurationException::ErrorCode error)
      : ROSException("Config", error, getErrorString(error)) {}

   inline SSIConfigurationException::SSIConfigurationException(SSIConfigurationException::ErrorCode error,
                                                               string description)
      : ROSException("Config", error, getErrorString(error), description) {}

   inline string SSIConfigurationException::getErrorString(unsigned int errorId) const{
      string result;

      switch (errorId) {
      case SSIConfigurationException::UNKNOWN_CONFIG_ERROR:
         result="Unknown Configuration";
         break;
      default:
         result="Unspecified error";
         break;
      }
      return(result);
   }

   class SSIConfiguration
   {
   public:
      typedef map<string, Config::T_ConfigContainer>::iterator SSIConfigIterator;

      SSIConfiguration(){};
      SSIConfiguration(const char *);

      Config::T_ConfigContainer getConfig(string key);
      void setConfig(string key,  Config::T_ConfigContainer conf);

      string serialize();
      void dump() ;
 
      void vectorize(string key, vector<string> & StringArray);
      void keys(vector<string> & keys);
   private:
      map <string, Config::T_ConfigContainer> m_ssiConfig;
   };
}
#endif
