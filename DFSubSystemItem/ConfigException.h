// -*- c++ -*-
// $Id$ 

#ifndef CONFIGEXCEPTION_H
#define CONFIGEXCEPTION_H

#include <string>

#include "DFExceptions/ROSException.h"

class ConfigException : public  ROSException
{
public:
   enum ErrorCode {UNKNOWN_KEY_ERROR,BAD_CONVERSION_ERROR};
   std::string getErrorString(unsigned int errorId) const;

   ConfigException(ErrorCode error);
   ConfigException(ErrorCode error, std::string description);
   ConfigException(ErrorCode error, const ers::Context& context);
   ConfigException(ErrorCode error, std::string description, const ers::Context& context);
   ConfigException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

protected:
   virtual ers::Issue * clone() const { return new ConfigException( *this ); }

   static const char * get_uid() { return "ROS::ConfigException"; }
   virtual const char* get_class_name() const {return get_uid();}
};

inline ConfigException::ConfigException(ConfigException::ErrorCode error)
   : ROSException("Config", error, getErrorString(error)) {}

inline ConfigException::ConfigException(ConfigException::ErrorCode error,
                                        std::string description)
   : ROSException("Config", error, getErrorString(error), description) {}

inline ConfigException::ConfigException(ConfigException::ErrorCode error, const ers::Context& context)
   : ROSException("Config", error, getErrorString(error), context) {}

inline ConfigException::ConfigException(ConfigException::ErrorCode error,
                                        std::string description, const ers::Context& context)
   : ROSException("Config", error, getErrorString(error), description, context) {}

inline ConfigException::ConfigException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "Config", error, getErrorString(error), description, context) {}

inline std::string ConfigException::getErrorString(unsigned int errorId) const{
   std::string result;

   switch (errorId) {
   case ConfigException::UNKNOWN_KEY_ERROR:
      result="Unknown key";
      break;
   case ConfigException::BAD_CONVERSION_ERROR:
      result="Conversion from string failed";
      break;
   default:
      result="Unspecified error";
      break;
   }
   return(result);
}
#endif
