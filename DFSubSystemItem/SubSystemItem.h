/*   SubSystemItem interface
     Authors: B. Gorini, G. Lehmann
     Date: %Date%
*/

#ifndef SUBSYSTEMITEM_H
#define SUBSYSTEMITEM_H

#include <map>
#include <iostream>
#include <string>

#include "DFSubSystemItem/SSIConfiguration.h"
#include "DFThreads/DFIdentifyble.h"
#include "DFSubSystemItem/Controllable.h"

class SubSystemItem : public DFIdentifyble, public Controllable {
public:
  SubSystemItem(std::string name);
  virtual ~SubSystemItem();

  // Methods related to getting and setting configuration
  // and statistics parameters
  virtual void setConfigPlugin(std::string & pluginName);
  virtual string getConfigPlugin();
  virtual void updateConfig(SSIConfiguration * conf);
  virtual void updateRunConfig(SSIConfiguration * runConf);
  virtual void updateStatistics(SSIConfiguration * runConf);

  virtual SSIConfiguration * getConfig();
  virtual SSIConfiguration * getRunConfig();
  virtual SSIConfiguration * getStatistics();

  virtual void clearConfig();
  virtual void clearRunConfig();
  virtual void clearStatistics();

  // Do NOT overload in controlled software applications!!
  virtual void init();
  virtual void uninit();

  //Flag to indicate if the item can be restarted dynamically
  virtual void setRestartable(bool restartable);
  virtual bool restartable();
  virtual void setEssential(bool essential);
  virtual bool essential();
  virtual void setActive(bool active);
  virtual bool active();

private:
  SSIConfiguration * m_configuration;
  SSIConfiguration * m_runParameters;
  SSIConfiguration * m_getInfoParameters;
  string m_pluginName;
  bool m_restartable;
  bool m_essential;
  bool m_active;

};
#endif //SUBSYSTEMITEM_H
