// -*- c++ -*-
// $Id$ 

#ifndef CONFIG_H
#define CONFIG_H

#include <stdint.h>
# include <sstream>

#include <map>
#include <vector>
#include <string>

#include "DFThreads/DFCountedPointer.h"


/** Simple key/value pair configuration class.
 */
namespace ROS {
   class Config{
   public:
      typedef std::vector <DFCountedPointer<Config> > T_ConfigContainer ;
      typedef std::map<std::string,std::string>::iterator ConfigIterator;
      typedef std::vector <DFCountedPointer<Config> >::iterator VectorIterator;
      typedef enum {STRING, INT, FLOAT, POINTER, VECTOR} ValueType;

      static DFCountedPointer<Config> New();
      static DFCountedPointer<Config> New(const char*);

      std::string serialize();
  
      void vectorize(std::vector<std::string> & );

      int          getInt(const std::string key, const int index=-1);
      unsigned int getUInt(const std::string key, const int index=-1);
      long unsigned int getLUInt(const std::string key, const int index=-1);
      int64_t      getS64(const std::string key, const int index=-1);
      uint64_t     getU64(const std::string key, const int index=-1);
      bool         getBool(const std::string key, const int index=-1);
      float        getFloat(const std::string key, const int index=-1);
      double       getDouble(const std::string key, const int index=-1);
      std::string       getString(const std::string key, const int index=-1);

      /** Get a pointer to a value of the given type.  Unfortunately
          there is no check that the stored pointer is of the type
          requested, we just have to use reinterpret_cast and trust it.
      */
      template <class Type>
      Type* getPointer(const std::string key);

      template <class Type>
      std::vector<Type> getVector(const std::string key);

      inline std::map<std::string, std::string>& getMap() {return m_map;}

      bool isKey(const std::string& key, const int index=-1);

      void set(const std::string key, const int value, const int index=-1);
      void set(const std::string key, const unsigned int value, const int index=-1);
      void set(const std::string key, const int64_t value, const int index=-1);
      void set(const std::string key, const uint64_t value, const int index=-1);
      void set(const std::string key, const float value, const int index=-1);
      void set(const std::string key, const double value, const int index=-1);
      void set(const std::string key, const std::string value, const int index=-1);
      template <class Type>
      void setVector(const std::string key, const std::vector<Type>& value);
      template <class Type>
      void setPointer(const std::string key, const Type* pointer) ;


      size_t size();

      void reset(void);
      void dump(void);

   private:
      const std::string genKey(const std::string key, const int index);
      ConfigIterator lookup(const std::string& key, const int index=-1);


   protected:
      Config();
      Config(const char*);

   private:
      std::map<std::string, std::string> m_map;
      std::map<std::string, ValueType> m_types;
   };

   // 13/03/03 RS, template function for getting vectors
   template <class Type>
   std::vector<Type> Config::getVector(const std::string key)
   {
      std::vector<Type>		value;
      Type 		typ;
      std::string			t(",");
      std::string::size_type p = 0;

      std::map<std::string, std::string>::iterator iter;
      int index=0;
      while ((iter=m_map.find(genKey(key,index))) != m_map.end()) {
         std::istringstream is(iter->second);
         is >> typ;			// does not work for all types!
         value.push_back(typ);
         index++;
      }
      if (index==0) {
         std::string s=getString(key);
         while((p=s.find(t)) != std::string::npos) {
            std::string v(s,0,p);
            s.erase(0,p+t.size());
            std::istringstream is(v);
            is >> typ;			// does not work for all types!
            value.push_back(typ);
         }
         if (s.size() > 0) {
            std::istringstream is(s);
            is >> typ;			// does not work for all types!
            value.push_back(typ);
         }
      }
      return(value);			// cannot avoid copy of local object
   }

   inline DFCountedPointer<Config> Config::New() {
      return (new Config);
   }
   inline DFCountedPointer<Config> Config::New(const char* confStream){
      return (new Config(confStream));
   }

   // 13/03/03 RS, template function for setting vectors
   template <class Type>
   void Config::setVector(const std::string key, const std::vector<Type>& value) 
   {
      std::ostringstream sStream;
      for(unsigned int i=0; i<value.size(); i++ )
         sStream << value[i] << ",";	// does not work for all types!
      m_map[key] = sStream.str();
   }

   template <class Type>
   void Config::setPointer (const std::string key, const Type* classPointer)
   {
      m_types[key]=POINTER;

      unsigned long pointer=reinterpret_cast<unsigned long>(classPointer);

      std::ostringstream sStream;
      sStream << pointer;
      m_map[key]=sStream.str();
   }

   template <class Type> 
   Type* Config::getPointer (const std::string key)
   {
      if (m_types[key]==POINTER) {
         std::string stringPointer=getString(key);
         std::istringstream sStream(stringPointer);
         unsigned long pointer;
         sStream >> pointer;

         Type* temp=reinterpret_cast<Type*>(pointer);
         return temp;
      }
      else {
         return 0;
      }
   }
}
#endif //CONFIG_H
