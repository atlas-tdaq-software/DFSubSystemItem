// -*- c++ -*-

/*
  ATLAS ROS Software

  Class: Controllable
  Authors: ATLAS ROS group 	
*/


#ifndef CONTROLLABLE_H
#define CONTROLLABLE_H

#include <string>

/** Generic base class for all objects that need to be notified of any FSM state change. */
class Controllable {
public:
  virtual void load() { }

  virtual void configure() { }

  virtual void prepareForRun() { }

  virtual void startTrigger() { }

  virtual void stopTrigger() { }

  virtual void stopFE() { }

  virtual void stopDC() { }

  virtual void stopEF() { }

  virtual void stopPT() { }

  virtual void stopSFO() { }

  virtual void pause() { }

  virtual void resume() { }

  virtual void probe() { }

  virtual void publishFullStatistics() { }

  virtual void unconfigure() { }

  virtual void unload() { }

  virtual void checkpoint() { }

  virtual void userCommand(std::string & commandName, std::string & argument) { }

  virtual void load(std::string & s) { this->load(); }

  virtual void configure(std::string & s) { this->configure(); }

  virtual void prepareForRun(std::string & s) { this->prepareForRun(); }

  virtual void startTrigger(std::string & s) { this->startTrigger(); }

  virtual void stopTrigger(std::string & s) { this->stopTrigger(); }

  virtual void stopFE(std::string & s) { this->stopFE(); }

  virtual void stopDC(std::string & s) { this->stopDC(); }

  virtual void stopEF(std::string & s) { this->stopEF(); }

  virtual void stopPT(std::string & s) { this->stopPT(); }

  virtual void stopSFO(std::string & s) { this->stopSFO(); }

  virtual void pause(std::string & s) { this->pause(); }

  virtual void resume(std::string & s) { this->resume(); }

  virtual void probe(std::string & s) { this->probe(); }

  virtual void publishFullStatistics(std::string & s) { this->publishFullStatistics(); }

  virtual void unconfigure(std::string & s) { this->unconfigure(); }

  virtual void unload(std::string & s) { this->unload(); }

  virtual void checkpoint(std::string & s) { this->checkpoint(); }

  virtual void userCommand(std::string & s) {   
    // Gymnsatics to distinguish between command and arguments
    std::string::size_type i1 = s.find_first_not_of(' ');

    if ( i1 == std::string::npos ) return;
    std::string::size_type i2 = s.substr(i1, s.size()).find_first_of(' ');
    std::string comdName = s.substr(i1,i2);

    std::string arguments; 
    if ( i2 != std::string::npos ) 
       arguments = s.substr(i2+1, s.size());

    this->userCommand(comdName, arguments); } 
};
#endif //CONTROLLABLE_H
